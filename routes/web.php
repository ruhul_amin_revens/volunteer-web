<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about-us', function () {
    return view('about');
})->name('about');
Route::resource('post','PostController');
Route::resource('userprofile','UserProfileController');
Route::get('donors','MiscController@donors')->name('donors');
Route::get('user/{id}','MiscController@user');
Route::get('post-by-user/{id}','MiscController@postBbyUser')->name('post-by-user');
Route::post('search-post','MiscController@search')->name('search-post');
