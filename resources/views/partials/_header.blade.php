<!DOCTYPE html>

<head>
    <title>VolunteerBD</title>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Volunteering Website">
    <meta name="author" content="Exceptional Programmers">

    <link rel="stylesheet" href="{{url('')}}/custom/css/styles.css">

    <!-- Bootstrap Link CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{url('')}}/custom/bootstrap/css/bootstrap.min.css">

    <!-- Custom Link CSS Files -->
    <link rel="stylesheet" href="{{url('')}}/custom/css/custom.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

</head>

@include('partials._nav')
