<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\User;
use Illuminate\Http\Request;

class MiscController extends Controller
{
    public function donors(){
        $donors = User::orderBy('id','asc')->paginate(10);
        return view('donors',compact('donors'));
    }
    public function search(Request $request){
        $posts = Post::where('city',$request->city)->where('type','like',$request->type)->get();
        return view('home',compact('posts'));
    }
    public function user($id){
        $user = User::where('id',$id)->firstOrFail();
        return view('userProfile',compact('user'));
    }
    public function postBbyUser($id){
        $user = User::where('id',$id)->firstOrFail();
        $posts = $user->posts();
        return view('home',compact('posts'));
    }
}
